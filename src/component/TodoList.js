import React, {Component} from "react";
import TodoItem from "./TodoItem"


class TodoList extends Component {
    render() {
      return (
        <section className="main">
          <ul className="todo-list">
            {this.props.todos.map((todo) => (
              <TodoItem title={todo.title} completed={todo.completed} deleteTodo={this.props.deleteTodo} id={todo.id} key={todo.id} toggleTodo={this.props.toggleTodo}/>
            ))}
          </ul>
        </section>
      );
    }
  }

  export default TodoList