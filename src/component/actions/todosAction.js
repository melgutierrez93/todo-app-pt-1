export const ADDTODO = "ADDTODO"

export const addTodo = (inputText) => {
    ({type:ADDTODO, inputText})
}