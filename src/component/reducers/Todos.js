import { todos as initialTodos} from "./todos"
import { ADDTODO } from "../actions/todosAction"
import {v4 as uuid} from "uuid"

export const todos= (state = initialTodos, action) => {
    switch(action.type){
        case ADDTODO: {
            const newId = uuid()
            const newTodo = {
                "userId":1,
                "id":newId,
                "tittle":action.payload.inputText,
                "completed":false

            }
            const neTodos={
                ...state, [newId]:newTodo
            }
            newTodos[newId]= {newTodo}
        
        }
        default:
             return state
    }
}