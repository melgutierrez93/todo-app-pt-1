import React, {Component} from "react";
import todosList from "./todos.json";
import TodoList from './component/TodoList';
import TodoItem from './component/TodoItem';
import {NavLink, Route, Switch} from "react-router-dom";
import {connect} from "react-redux"
import "./App.css" 


class App extends Component {
  state = {
    todos: todosList,
  };z

  handleAddTodo = (event) => {
    if (event.key === "Enter") {
      let newTodo = { title: event.target.value, userId: 1, id: Math.random() * 10000, completed: false }

      this.setState({ todos: [...this.state.todos, newTodo] })

      event.target.value = ""
    }
  }
  handleDelete = todoId => {
    console.log(todoId)
    const newTodos = this.state.todos.filter(
      todo => todo.id !== todoId
    )
    this.setState({ todos: newTodos })
  }
  handleToggle = todoId => {
    let newTodos = this.state.todos.map(todo => {
      if (todo.id === todoId) {
        return {
          ...todo,
          completed: !todo.completed
        }
      }
      return { ...todo }
    })
    this.setState({ todos: newTodos })
  }
  handleClear = () => {
    const newTodos = this.state.todos.filter(
      todo => todo.completed === false
    )
    this.setState({ todos: newTodos })

  }

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input className="new-todo" placeholder="What needs to be done?" autofocus onKeyPress={this.handleAddTodo} />
        </header>
        <Switch>
          <Route exact path="/" render={(props)=> <TodoList
           todos={this.state.todos} 
           deleteTodo={this.handleDelete}
          toggleTodo={this.handleToggle} /> }/>

          <Route exact path="/active" render={(props)=> <TodoList
           todos={this.state.todos.filter(todo => todo.completed === false)} 
           deleteTodo={this.handleDelete}
          toggleTodo={this.handleToggle} /> }/>

          <Route exact path="/completed" render={(props)=> <TodoList
           todos={this.state.todos.filter(todo => todo.completed === true)} 
           deleteTodo={this.handleDelete}
          toggleTodo={this.handleToggle} 
          /> }/>
        </Switch>
        
        <footer className="footer">
          <span className="todo-count">
            <strong>{this.state.todos.filter(todo => todo.completed === false).length}</strong> item(s) left
          </span>
          <ul className="filters">
            <li>
              <NavLink exact activeClassName ="selectLink" to="/">All</NavLink>
            </li>
            <li>
              <NavLink exact activeClassName ="selectLink" to="/active">Active</NavLink>
            </li>
            <li>
              <NavLink exact activeClassName ="selectLink" to="/completed">Completed</NavLink>
            </li>
          </ul>
          <button className="clear-completed" onClick={this.handleClear}>Clear completed</button>
        </footer>
      </section>
    );
  }
}

const mapStateToProps = (state) => { ({
  todos: state.todos
})
}
const mapDispatchToProps = (dispatch) => ({
  addTodo: inputText => dispatch(addTodo(inputText))
})





export default connect (mapStateToProps, mapDispatchToProps)(App);
